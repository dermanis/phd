# Photonic RC cryptography

* [next: Linear Regression](./linear_regression.md)

## Inputs

Inputs occur from a pseudo-chaotic sequence. The process is deterministic, so they are reproducible but a minor change of the initial conditions result to a major difference of the final outcome.

![Sample Inputs](./figures/sample_inputs.png)
*Fig.1: Inputs - Samples of different input timeseries*


## Outputs

The inputs mentioned above, are fed to an array of MMRs and the output is read by an array of PDs. The outputs are normalized. The results that follow occur by a simulation, so the outputs have 64-bit precision and thus are considered to be analog values. Later on a more realistic digital scenario is presented.

Different challenges result to different (normalized) outputs of the photonic reservoir.

![Sample Outputs](./figures/sample_outputs.png)
*Fig.2: Outputs - Samples of different (normalized) output timeseries for a specific reservoir output*


## Input - Output cross-correlation

Each output of the reservoir has strong correlation with the input timeseries as shown below. Each output has a different lag from the input. **PROBLEM?**

![Sample Input Output](./figures/inout_xcorr.png)
*Fig.3: Input - Output cross-correlation for different reservoir outputs*


## Outputs (digital)

As mentioned above, the outputs occur from a simulation and are calculated with a 64-bit precision. In a more realistic scenario the ADCs will have limited accuracy. When the outputs are sampled to digital values the result depends on how many bits are used for the sampling process.

![Sample Output digital](./figures/sample_outputs_q.png)
*Fig.4: Outputs after a 8-bit ADC*

The following table shows the mean squarred error for different ADC precisions.

| bit | levels |        step        |         mse         |
|-----|--------|--------------------|---------------------|
|  8  |   256  | ~4 10<sup>-3</sup> | ~5 10<sup>-6</sup>  |
| 10  |  1024  | ~1 10<sup>-3</sup> | ~3 10<sup>-7</sup>  |
| 12  |  4096  | ~2 10<sup>-4</sup> | ~2 10<sup>-7</sup>  |
| 16  | 65535  | ~2 10<sup>-5</sup> | ~8 10<sup>-11</sup> |

It seems that a common ADC with 8-bit precision is adequate for sampling each PD output.

---

* [next: Linear Regression](./linear_regression.md)

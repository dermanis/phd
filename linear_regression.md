# Photonic RC cryptography

* [previous: Photonic Reservoir](./photonic_rc.md)
* [next: Binary secret](./binary.md)

## Linear Regression - Strong PUF

### Linear Regression process

A regression method is used in order to match the reservoir's outputs with the desired outputs (teacher). The method used is Tikhonov regularization (ridge regression) where the weights are calculated using the following formult, where **X** are the outputs of the reservoir and **y** are the desired outputs. Factor **λ** is used to adjust overfitting / underfitting and stabilize the solution.

![Tikhonov regularization](./figures/tikhonov.png)
*Fig.1: Formula used for weight calculation*


### Weights

The weights produced by the linear regression algorithm are different for different inputs.

![Weights for different challenges](./figures/weights_per_challenge_8bit.png)
*Fig.2: Weights for different challenges*

Considering the weights of each of the challenges an a vector, the euclidean distance of each weight with all the others is calculated. The mean euclidean distance is 10.1 with a standard deviation of 1.1 while if the calculation would be with a totally random vector the mean distance would be 6.9 with a standard deviation of 0.3


### Weights distribution

The weigts produced by 10,000 simulations shows that the weights distribution resembles the normal distribution. This means that it is possible to produce values following uniform distribution out of these weights.

![Weights distribution](./figures/weight_distribution.png)
*Fig.3: Weights distribution for 10,000 different challenges*


### Uniform random numbers

In order to produce a uniform distribution it is possible to use the CDF of the distribution and then calculate each weight's probability. This gives a number in (0,1). The uniform distribution of these numbers is shown in the following figure, along with a comparison of random numbers produced by the NumPy library generator.

![Random numbers (256 bins)](./figures/rand_256.png)
*Fig.4: Histogram (256 bins) of random numbers produced by 10,000 different challenges*

![Random numbers (1024 bins)](./figures/rand_1024.png)
*Fig.5: Histogram (1024 bins) of random numbers produced by 10,000 different challenges*

![Random numbers (4096 bins)](./figures/rand_4096.png)
*Fig.6: Histogram (4096 bins) of random numbers produced by 10,000 different challenges*

![Random numbers (16834 bins)](./figures/rand_16384.png)
*Fig.7: Histogram (16834 bins) of random numbers produced by 10,000 different challenges*

![Random numbers (65536 bins)](./figures/rand_65536.png)
*Fig.7: Histogram (65536 bins) of random numbers produced by 10,000 different challenges*

---
* [previous: Photonic Reservoir](./photonic_rc.md)
* [next: Binary secret](./binary.md)

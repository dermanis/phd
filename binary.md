# Photonic RC cryptography

* [previous: Linear Regression](./linear_regression.md)

## Binary Secret - Strong PUF

### Random numbers to binary sequence

The random numbers produced in the previous step are processed in order to obtain a random binary sequence. It is possible to extract many bits per weight, but more bits per weight means that the binary sequence is more susceptible to noise, even if the effects of noise are strongly mitigated by the ADC. In short more bits / weight will make the secret more random but less possible to reproduce. If the aim is a totally random number, then the more the better.

The process is pretty simple:
* The number of bits per weight *n*, is specified.
* The range (0,1) is sliced in 2<sup>*n*</sup> bins.
* Each number is assigned to a bin according to its value.
* The number of the bin [0, 2<sup>*n*-1</sup>] is encoded in binary using RB code.

#### Reflected Binary code (Gray code)
The use of Gray code was chosen so adjacent bins differ by just one bit. This way the bit flips caused by the noise will affect the binary sequence the least possible.


### Hamming distances

Different challenges will result in very different responses (binary sequences). Comparing the hamming distance between the responses shows that they are very different one with another, even if few bits per weight are used.
The figure below shows the mean hamming distance of every response with every other response and the standard deviation.
![Hamming distance for different bits/weight](../figures/hamming_distance_strong.png)
*Fig.1: Hamming distance for different bits/weight*


### NIST statistical test suite

To verify the statistical randomness, the NIST statistical test suite is used. Responses from many different challenges are stitched together to create one very long binary sequence. This sequece is tested against the NIST tests.


#### NIST sts results (1)

For this first test the bits produced are tested against all possible tests and pass them all except the univesral test. This particular test needs a very large number of bits to run and producing that many by simulation would be extremely time consuming. The tests are repeated though so that they pass all the NIST sts tests, see below.


```

------------------------------------------------------------------------------
RESULTS FOR THE UNIFORMITY OF P-VALUES AND THE PROPORTION OF PASSING SEQUENCES
------------------------------------------------------------------------------
   generator is <./data/tq8n16.txt>
------------------------------------------------------------------------------
 C1  C2  C3  C4  C5  C6  C7  C8  C9 C10  P-VALUE  PROPORTION  STATISTICAL TEST
------------------------------------------------------------------------------
 68  96  93  76 112 121 114  94 125 101  0.000287    996/1000    Frequency
 61  84 101  96  90 109 117 107 110 125  0.000378    997/1000    BlockFrequency
 70  65  88  95 105  94 112 104 113 154  0.192724    998/1000    CumulativeSums
 66  75  97 103  92 109 100 104 106 148  0.180568    995/1000    CumulativeSums
 90  81 104  96 111 117  91 110  92 108  0.206629    995/1000    Runs
118  89 112 102  88  91  93 115  91 101  0.228367    988/1000    LongestRun
 95 123 100  96  90 100 103  88 105 100  0.486588    987/1000    Rank
140 110  83  80 114 107  62 134  61 109  0.003061    985/1000    FFT
 53  65  72  84 111 108 114 112 141 140  0.080519    997/1000    NonOverlappingTemplate
 52  88  83  85 106  98 126  97 126 139  0.249284    996/1000    NonOverlappingTemplate
103  86  89 108  89 109  92 117 109  98  0.326749    989/1000    NonOverlappingTemplate
 76  88  96  94  91  91 109 124 107 124  0.008385    996/1000    NonOverlappingTemplate
104  79  89 110 110 117  96  99 101  95  0.268917    982/1000    NonOverlappingTemplate
104  97 112  96  80  98 112 100  94 107  0.516113    989/1000    NonOverlappingTemplate
 72  82 102 108  91  99  96 122 113 115  0.010531    997/1000    NonOverlappingTemplate
 96  82  72 112  98 101 114 103 111 111  0.045675    990/1000    NonOverlappingTemplate
109  95  86 101 112  97  97 109 100  94  0.757790    989/1000    NonOverlappingTemplate
 87  95 108 100  98 105  97  98 118  94  0.678686    989/1000    NonOverlappingTemplate
 94  99  93  88 105 113  95 106 120  87  0.308561    985/1000    NonOverlappingTemplate
 98 106  91  87 103  91 112  90 111 111  0.469232    982/1000    NonOverlappingTemplate
 99 109  92  95 107  96  99 115  86 102  0.676615    989/1000    NonOverlappingTemplate
 95  96  96  87 104 103 103  98  94 124  0.459717    989/1000    NonOverlappingTemplate
 94 107  96 105 101  97 100 109  98  93  0.975012    988/1000    NonOverlappingTemplate
 79  84  96  99 124 101  80 108 114 115  0.009672    992/1000    NonOverlappingTemplate
 99 109 107  88  84  98 104 103  98 110  0.674543    991/1000    NonOverlappingTemplate
104 110 102 113 102  95  85  95  93 101  0.721777    990/1000    NonOverlappingTemplate
 99  82 115 103  91  95 107 107 101 100  0.570792    989/1000    NonOverlappingTemplate
108 100  99 106 101  96 104  94  89 103  0.964295    989/1000    NonOverlappingTemplate
119  85  97 102  83  99 109 103  88 115  0.142062    988/1000    NonOverlappingTemplate
 94 104 120  92  86  97  99  88 118 102  0.216713    991/1000    NonOverlappingTemplate
 92  96  96  98 107  96 117  92 109  97  0.731886    991/1000    NonOverlappingTemplate
101  97 101  85 107 106  98 104  95 106  0.910091    981/1000    NonOverlappingTemplate
108 106 106  94 100  96  93  89 110  98  0.866097    994/1000    NonOverlappingTemplate
 99 114 104  78  95 115  98 100  95 102  0.366918    990/1000    NonOverlappingTemplate
118  97  91  88 112  83 104 100 113  94  0.206629    990/1000    NonOverlappingTemplate
106 103 106 107 101  99 100  81  96 101  0.825505    990/1000    NonOverlappingTemplate
119  99  98  93  92  91 118  90 109  91  0.245490    987/1000    NonOverlappingTemplate
109 101  97  97  91 116  98 102  74 115  0.142872    996/1000    NonOverlappingTemplate
 98 101  96 109 103  95 107 100  96  95  0.986658    990/1000    NonOverlappingTemplate
101 102 112 106  87  98  93  98 108  95  0.834308    987/1000    NonOverlappingTemplate
110 102 105  69  86 101 116 119 108  84  0.008149    984/1000    NonOverlappingTemplate
114  89 104  93 106 103  92 106  82 111  0.373625    984/1000    NonOverlappingTemplate
102  83 104  94 103  94  92 114 103 111  0.554420    990/1000    NonOverlappingTemplate
 99 101  96  92  93 113 104  97 106  99  0.934599    987/1000    NonOverlappingTemplate
 96 112  94 102  91 110 110 108  77 100  0.293952    980/1000    NonOverlappingTemplate
 91  94 100  91 103  99  95 100 125 102  0.473064    991/1000    NonOverlappingTemplate
108 112  98  95 100  93 113  92  94  95  0.759756    989/1000    NonOverlappingTemplate
 98  94 110  90 100 103 106 104 101  94  0.947308    991/1000    NonOverlappingTemplate
 97 102 104  94  97 108 112 101  98  87  0.870856    993/1000    NonOverlappingTemplate
111  89  89 115 101 107  92 113 102  81  0.193767    985/1000    NonOverlappingTemplate
 98 105 109  99  93 101  87  93 113 102  0.786830    988/1000    NonOverlappingTemplate
 94  96  95 106  95  93 103 102 111 105  0.943242    984/1000    NonOverlappingTemplate
 97  97 113 104 100  92  80 116  91 110  0.272977    993/1000    NonOverlappingTemplate
102  97 103  92 115  98  85 101 102 105  0.769527    986/1000    NonOverlappingTemplate
107  89  87 104 102 118  89 112  96  96  0.366918    987/1000    NonOverlappingTemplate
 95 104  99 104  92  87  93 116  99 111  0.618385    984/1000    NonOverlappingTemplate
 89 126  96  94  96 106  91  99 109  94  0.275709    990/1000    NonOverlappingTemplate
 93  95  96 105 107  98 102  97 102 105  0.989786    986/1000    NonOverlappingTemplate
112  93  88 108  99 106 100  87 108  99  0.666245    984/1000    NonOverlappingTemplate
 84  95  98  93  90 114 112  99 108 107  0.448424    993/1000    NonOverlappingTemplate
 97 105 104  99  98 107 100  79 115  96  0.548314    992/1000    NonOverlappingTemplate
 91 103 115  87 102 120 105 107  86  84  0.117432    993/1000    NonOverlappingTemplate
 98  95 107  97  83  98 109 123 100  90  0.282626    988/1000    NonOverlappingTemplate
 94  97  91  99  99  97  96  95 116 116  0.647530    992/1000    NonOverlappingTemplate
 94  94  87 100 111  94 117  91 101 111  0.446556    989/1000    NonOverlappingTemplate
108  87 100 116 105 104  85  88 101 106  0.404728    989/1000    NonOverlappingTemplate
 91 102  90 113  89 102 109 106 100  98  0.739918    994/1000    NonOverlappingTemplate
 93  95  98 117  99 100  99 104 100  95  0.904708    989/1000    NonOverlappingTemplate
110  94 108  90 109 107  89 107  93  93  0.639202    986/1000    NonOverlappingTemplate
 99 107  98 105 110 100  84  86 106 105  0.645448    989/1000    NonOverlappingTemplate
104  87  87 113 110  99  98  83 126  93  0.058612    989/1000    NonOverlappingTemplate
100  90  97 101 103  99  98  93 105 114  0.915317    988/1000    NonOverlappingTemplate
100  97  95 102  90  95 109  99 108 105  0.949278    991/1000    NonOverlappingTemplate
 88  78 114 116  93  97 116  99 102  97  0.119508    992/1000    NonOverlappingTemplate
 90  95 101  97  96  95  94 113 113 106  0.753844    991/1000    NonOverlappingTemplate
 98 101  93 100 103  99 105  96 108  97  0.994488    993/1000    NonOverlappingTemplate
102  94 102  99 111 100 107  88  94 103  0.908760    988/1000    NonOverlappingTemplate
 92  91  96  99 119  95  89 106 103 110  0.520102    990/1000    NonOverlappingTemplate
 81 105 103 108  98 108  89 111  93 104  0.500279    995/1000    NonOverlappingTemplate
 89  86  89  92 122 104 107  95 106 110  0.206629    992/1000    NonOverlappingTemplate
115  95  84 103 109 102  97  94  98 103  0.680755    986/1000    NonOverlappingTemplate
 99  91 109  95 111 106  93  98  96 102  0.899171    991/1000    NonOverlappingTemplate
 53  65  71  85 110 109 114 112 139 142  0.002357    997/1000    NonOverlappingTemplate
113  90 107  94 103 100  93 109  99  92  0.781106    992/1000    NonOverlappingTemplate
107  86  94  85 105 120 100 118  96  89  0.125200    987/1000    NonOverlappingTemplate
 92  95 114  79 103  94  93 113  94 123  0.077131    992/1000    NonOverlappingTemplate
108 106  81 102 112  92  93 102 107  97  0.550347    991/1000    NonOverlappingTemplate
 90  96 103 110 102 105  96 111 100  87  0.779188    990/1000    NonOverlappingTemplate
102 100 104 103  93  82 109 103  92 112  0.637119    994/1000    NonOverlappingTemplate
102 102 111 111  90  87  91 111 107  88  0.424453    987/1000    NonOverlappingTemplate
 84  93 104 103 113 100 102  84 106 111  0.422638    993/1000    NonOverlappingTemplate
 99 100  90  95 115  97 104 100  87 113  0.622546    986/1000    NonOverlappingTemplate
100  91 100 105  80  92 109 123  91 109  0.144504    988/1000    NonOverlappingTemplate
117  96  99  85  88 100  96 119  97 103  0.296834    989/1000    NonOverlappingTemplate
113 102  75 116  84 110 111 101  95  93  0.065639    992/1000    NonOverlappingTemplate
110 106 102  97  93  98 108  91  89 106  0.830808    989/1000    NonOverlappingTemplate
106  92 113  96  91 106 101  96 103  96  0.880145    988/1000    NonOverlappingTemplate
100 109  95 105  90 103  91 118 100  89  0.568739    991/1000    NonOverlappingTemplate
 87 100  94  93 116 101  99 101 116  93  0.516113    993/1000    NonOverlappingTemplate
 95  84  88 111  97  82 117  84 120 122  0.006019    991/1000    NonOverlappingTemplate
120 104  90 106  98 109 109  97  79  88  0.157251    992/1000    NonOverlappingTemplate
 98 117  96 114 102  79 107  89  88 110  0.135720    990/1000    NonOverlappingTemplate
 98  90  96 119  99 109  95  83 103 108  0.392456    985/1000    NonOverlappingTemplate
 82  98 108  97 103 108  96 117  91 100  0.474986    991/1000    NonOverlappingTemplate
 58  99  93  88  97  99 109 110 127 120  0.000146    996/1000    NonOverlappingTemplate
 77  89 100 119 100 118 102 106 102  87  0.078567    991/1000    NonOverlappingTemplate
106 104  95  81 105  97  93 118 100 101  0.488534    990/1000    NonOverlappingTemplate
103  98  95  97  99 105  93 104 105 101  0.995969    990/1000    NonOverlappingTemplate
 95 101 101  99 116  94  95 114  88  97  0.643366    988/1000    NonOverlappingTemplate
110  96  93 117 113  99  93  92 101  86  0.406499    985/1000    NonOverlappingTemplate
112 105 117  99  93  96  97 101  93  87  0.583145    989/1000    NonOverlappingTemplate
 86  91 113 100  84 106 121  91 106 102  0.162606    988/1000    NonOverlappingTemplate
110  92 101 117 102  90 101  86  94 107  0.494392    984/1000    NonOverlappingTemplate
 94  94  96  95 119 109  88  96 103 106  0.574903    994/1000    NonOverlappingTemplate
102 106  97 111  92  88  93 100 102 109  0.823725    984/1000    NonOverlappingTemplate
 98  95  91 105  99 112 112  92  84 112  0.448424    988/1000    NonOverlappingTemplate
 95 115  96 102 106  86  81  94 116 109  0.193767    988/1000    NonOverlappingTemplate
101  99  88 102 116  99  89 116  95  95  0.500279    991/1000    NonOverlappingTemplate
 84  84 111  96 100 105 116 101  97 106  0.370262    990/1000    NonOverlappingTemplate
 98 103  99 103  97  87 111 109 102  91  0.844641    989/1000    NonOverlappingTemplate
111 101 105  90  89  94  92  91 115 112  0.420827    994/1000    NonOverlappingTemplate
108  92  98  96 111  93 103 100 101  98  0.950247    991/1000    NonOverlappingTemplate
 83  84 105 107 110  87 110 105 110  99  0.266235    990/1000    NonOverlappingTemplate
102 107  85 103 107 108 108  96 103  81  0.484646    987/1000    NonOverlappingTemplate
112 102  82 105 100  96  98 101  95 109  0.715679    992/1000    NonOverlappingTemplate
 99 104 105 103 105 111 102  88  85  98  0.765632    984/1000    NonOverlappingTemplate
 89  85 102  84  96 101  99 103 122 119  0.097159    992/1000    NonOverlappingTemplate
 97 106 103  89  99 109  94 125  91  87  0.231956    990/1000    NonOverlappingTemplate
 96  89 100  83  91 120 111 102 106 102  0.295391    986/1000    NonOverlappingTemplate
101  80  91 101  98 101 112  98 115 103  0.465415    985/1000    NonOverlappingTemplate
101 106  94  98 104  96 100 100 103  98  0.998740    988/1000    NonOverlappingTemplate
127  91 104  97  90  95 100  86 101 109  0.192724    992/1000    NonOverlappingTemplate
101  98 110  98  94  98 101 108 105  87  0.906069    993/1000    NonOverlappingTemplate
107 107 109 107 100  84 102 100 103  81  0.476911    990/1000    NonOverlappingTemplate
 92 101  96  96 101 108  88 103 103 112  0.861264    991/1000    NonOverlappingTemplate
110  92 104  99  85  95 110  93 101 111  0.635037    985/1000    NonOverlappingTemplate
109  96 118  87  95 103  98 105  89 100  0.560545    995/1000    NonOverlappingTemplate
104 118 103 110  78  96  85 110  96 100  0.167184    988/1000    NonOverlappingTemplate
 93 118 110  98 106  93  90 104  97  91  0.566688    988/1000    NonOverlappingTemplate
 97  92 112 107  90  90 107 118 105  82  0.220159    990/1000    NonOverlappingTemplate
 85  95 105 108  78  97 106 107 108 111  0.274341    992/1000    NonOverlappingTemplate
102  94 101 111 105 104  97  77 100 109  0.512137    987/1000    NonOverlappingTemplate
108  90 100  98 110  96 107  92 100  99  0.912724    982/1000    NonOverlappingTemplate
 92 109 100 103  93 101 102  97 105  98  0.981940    990/1000    NonOverlappingTemplate
 91 101  90  88 109 100 119  94 106 102  0.490483    994/1000    NonOverlappingTemplate
100  98  90 103 117  99  93 102  99  99  0.869278    990/1000    NonOverlappingTemplate
 91  97 106  98 103 104 104  95 105  97  0.985788    991/1000    NonOverlappingTemplate
 95  92 114  99  96 110 104  82  97 111  0.463512    990/1000    NonOverlappingTemplate
 87 101 104 112 114  99  77 107  83 116  0.057146    991/1000    NonOverlappingTemplate
113 103 100  84  83 100  89 106 108 114  0.249284    990/1000    NonOverlappingTemplate
 80 109 111  88  90 105 133  85  98 101  0.009201    988/1000    NonOverlappingTemplate
 81 110 104  88  99  98 101 126  90 103  0.118120    994/1000    NonOverlappingTemplate
 99 110 102  79 111  90 110 100  89 110  0.284024    990/1000    NonOverlappingTemplate
 95 105  91 101  97 103 107 108  91 102  0.942198    991/1000    NonOverlappingTemplate
 98 110  92  83  84 112 102 115 105  99  0.267573    986/1000    NonOverlappingTemplate
 99  90 111  93 110 107  93  98  98 101  0.853049    991/1000    NonOverlappingTemplate
 82 109 111 105 102 109 109  84  92  97  0.314544    985/1000    OverlappingTemplate
 96 124 102 101 105 105  93  88 105  81  0.199045    986/1000    ApproximateEntropy
 16  17  15   9  16  22  11  22   8  21  0.065502    154/157     RandomExcursions
  9  30  16  15  14  19  15  17  14   8  0.008266    155/157     RandomExcursions
 13  25  14  15  12  15  17  17  15  14  0.514124    154/157     RandomExcursions
 15  25  15  15  18  14  17  14  13  11  0.437274    157/157     RandomExcursions
 19  16  18  15  10  19   7  15  23  15  0.162606    155/157     RandomExcursions
 16  19  20  13  14  12  15  19  14  15  0.845773    157/157     RandomExcursions
 10  16  15  21  20  14  14  16  10  21  0.345115    155/157     RandomExcursions
 20  13  12  18   7  19  15  16  16  21  0.275709    154/157     RandomExcursions
 18  17  11  20  15  17  14  18  14  17  0.923314    160/161     RandomExcursionsVariant
 16  17  12  13  23  15  13  14  20  18  0.656634    160/161     RandomExcursionsVariant
 14  15  18  15  16  13  14  20  21  15  0.907251    159/161     RandomExcursionsVariant
 12  17  21  14  11  22  12  24  12  16  0.177264    159/161     RandomExcursionsVariant
 15  20  16  14  19  19  14  18  13  13  0.907251    159/161     RandomExcursionsVariant
 21  12  16  21  21  11  12  18  17  12  0.387049    158/161     RandomExcursionsVariant
 19  18  15  24  12  14  15  11  14  19  0.478598    158/161     RandomExcursionsVariant
 15  23  23  15   8  20  17  17  14   9  0.099885    158/161     RandomExcursionsVariant
 15  13  25  12  19   9  18  16  17  17  0.297739    159/161     RandomExcursionsVariant
 14  12  16  16  12  27  16  10  21  17  0.133884    160/161     RandomExcursionsVariant
 19  15  10  17  16  14  14  22  15  19  0.708280    161/161     RandomExcursionsVariant
 17  15  12  12  16  16  14  21  21  17  0.782780    161/161     RandomExcursionsVariant
 17  13  15  16  17  14  23  18  15  13  0.839722    161/161     RandomExcursionsVariant
 13  13  24  16  18  13  12  22   4  26  0.003661    160/161     RandomExcursionsVariant
 12  19  20  14  21  20  18   8  16  13  0.335520    161/161     RandomExcursionsVariant
 15  14  23  26  12  13  13  20  11  14  0.103676    161/161     RandomExcursionsVariant
 14  19  27  15  19  14   6  14  17  16  0.070902    161/161     RandomExcursionsVariant
 14  22  21  25  12  12   9  12  18  16  0.079599    160/161     RandomExcursionsVariant
 93  98  98 102  82  96 113 115 102 101  0.534146    990/1000    Serial
 89 107 118 103  95  94  97 105  88 104  0.576961    987/1000    Serial
116 105  70 101 109 100  90 102 102 105  0.123755    986/1000    LinearComplexity


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
The minimum pass rate for each statistical test with the exception of the
random excursion (variant) test is approximately = 980 for a
sample size = 1000 binary sequences.

The minimum pass rate for the random excursion (variant) test
is approximately = 155 for a sample size = 161 binary sequences.

For further guidelines construct a probability table using the MAPLE program
provided in the addendum section of the documentation.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

```


#### NIST sts results (2)

For this test the initial produced bits are used and augmented with the same bits in a random order resulting in a sequence large enought to pass all the NIST sts tests. If the initial bits are random, any permutation of them will also be random and vice versa.


```

------------------------------------------------------------------------------
RESULTS FOR THE UNIFORMITY OF P-VALUES AND THE PROPORTION OF PASSING SEQUENCES
------------------------------------------------------------------------------
   generator is <./data/tq8n16e.txt>
------------------------------------------------------------------------------
 C1  C2  C3  C4  C5  C6  C7  C8  C9 C10  P-VALUE  PROPORTION  STATISTICAL TEST
------------------------------------------------------------------------------
119 102 116 113  98  86 112  80  88  86  0.027497    990/1000    Frequency
 76  95 118  94 105 102  99  98  98 115  0.200115    991/1000    BlockFrequency
116  98 119 112 103  90  94  85  95  88  0.160805    988/1000    CumulativeSums
109 103 104 107  90  99 104  80 107  97  0.605916    987/1000    CumulativeSums
 98 101 103 106  99  91 103 101  96 102  0.996155    992/1000    Runs
100 106  92  97  94 117  94  95 106  99  0.805569    991/1000    LongestRun
102  99  93 102 116  97 113  86  89 103  0.516113    986/1000    Rank
202  89  85  87 104  96  74 103  94  66  0.098920    980/1000    FFT
 76  98  94 118 108 105  87 120 106  88  0.037813    993/1000    NonOverlappingTemplate
 84 109  87 112 110  96  97  79 111 115  0.075254    987/1000    NonOverlappingTemplate
103 101 107 105 102  99  96 108  87  92  0.910091    990/1000    NonOverlappingTemplate
104 109 102  88  93 110  89  98 106 101  0.783019    989/1000    NonOverlappingTemplate
110  95  95  90 107 106 107  83 111  96  0.524101    992/1000    NonOverlappingTemplate
106 115 124  77  86 100  88 115  86 103  0.011144    994/1000    NonOverlappingTemplate
 98  95  96  89  99 115  94 103 104 107  0.832561    994/1000    NonOverlappingTemplate
 86 104 104  95 111  94 101 113  93  99  0.709558    992/1000    NonOverlappingTemplate
109 110  85 106 115  87  82 108 109  89  0.113372    988/1000    NonOverlappingTemplate
102  97  97 121  94 118 110  82  95  84  0.083526    986/1000    NonOverlappingTemplate
 96 104  91 106  89  93 104  93 120 104  0.534146    992/1000    NonOverlappingTemplate
 88 102 104 101 111  96  98 104 103  93  0.924076    991/1000    NonOverlappingTemplate
106 120  97  87  99 103  90 110  87 101  0.355364    986/1000    NonOverlappingTemplate
109 108 104  91  94 119  95  94  95  91  0.528111    990/1000    NonOverlappingTemplate
122  91 112  99  98  91 101 109  92  85  0.233162    988/1000    NonOverlappingTemplate
104  92 107  97 108  98 110  86  97 101  0.823725    988/1000    NonOverlappingTemplate
 97  97 108  93  98 114 105  97 101  90  0.862883    993/1000    NonOverlappingTemplate
103  88 116  93 120 102 108 100  79  91  0.106246    987/1000    NonOverlappingTemplate
 96 116  93 101  99  98  99 114  98  86  0.612147    987/1000    NonOverlappingTemplate
100  98  91 110  89 111 105  99  99  98  0.869278    987/1000    NonOverlappingTemplate
114 113 109  98  82  92  94  94 104 100  0.413628    987/1000    NonOverlappingTemplate
101  94 110 111  99  82 116  98  97  92  0.422638    987/1000    NonOverlappingTemplate
 93  89 109 106  96  90  98 110  98 111  0.707513    986/1000    NonOverlappingTemplate
101 102  92  94 109 111 102  98  95  96  0.937919    994/1000    NonOverlappingTemplate
122  96  99  88 100  83 112 110 102  88  0.151190    990/1000    NonOverlappingTemplate
 85  91 102 114 103 101 107  94 102 101  0.733899    993/1000    NonOverlappingTemplate
107  85 108  98  75 103  96 117 101 110  0.128874    990/1000    NonOverlappingTemplate
 96  96 109 100  84  96 111  99 106 103  0.786830    995/1000    NonOverlappingTemplate
 87 113 106 108  95 108 103 110  90  80  0.251837    993/1000    NonOverlappingTemplate
122  94  91  87 110 117  98  89 111  81  0.039329    991/1000    NonOverlappingTemplate
 99 108 110 107  87 102 100  95  85 107  0.651693    990/1000    NonOverlappingTemplate
 90  97 108 101  79 103 107 104 111 100  0.524101    991/1000    NonOverlappingTemplate
103  86 111 111 103 107 100 104  88  87  0.500279    994/1000    NonOverlappingTemplate
117 109 111 105 111 100  90  83  85  89  0.132640    985/1000    NonOverlappingTemplate
 92 104 110  93  90 103  89  91 114 114  0.408275    994/1000    NonOverlappingTemplate
 84  98  92  99  90 110 107  97 119 104  0.383827    994/1000    NonOverlappingTemplate
 95  98 107 100 117 106  98  87 100  92  0.699313    987/1000    NonOverlappingTemplate
102 106  87 105  97 108  98  91  94 112  0.767582    993/1000    NonOverlappingTemplate
102  96 101 100  98  91  97 109  94 112  0.926487    982/1000    NonOverlappingTemplate
114 100 104 105  99  99  94  98  96  91  0.926487    988/1000    NonOverlappingTemplate
102 104 109  98  98 101 106  83 108  91  0.759756    985/1000    NonOverlappingTemplate
 99  99  99 103  86 102 107 105 100 100  0.969588    990/1000    NonOverlappingTemplate
100  91  89 113 117  85 115 102  86 102  0.156373    989/1000    NonOverlappingTemplate
100 119  80  91 103  87 108 105 108  99  0.228367    985/1000    NonOverlappingTemplate
 96 103  97  98 104 106 102 104  96  94  0.996155    989/1000    NonOverlappingTemplate
120 107 100  94  84  84 108 107  99  97  0.262249    988/1000    NonOverlappingTemplate
102 100  90 114  88 112  80 102 108 104  0.295391    994/1000    NonOverlappingTemplate
 97  97 108 101  86  95  99 101 109 107  0.886162    994/1000    NonOverlappingTemplate
 99  93 110  99  87  92 115 100 106  99  0.693142    991/1000    NonOverlappingTemplate
108 100  95  81 110  84 107 100 109 106  0.373625    994/1000    NonOverlappingTemplate
121  91  86  88  84 105  96 111 103 115  0.087162    989/1000    NonOverlappingTemplate
 97  94  87 115 111 119 102  84  88 103  0.147815    993/1000    NonOverlappingTemplate
 82  92  93  94 111 102  95 105 109 117  0.336111    990/1000    NonOverlappingTemplate
110 108 102 109  92 106  90  93  84 106  0.544254    990/1000    NonOverlappingTemplate
 97  94  94  89  93 108 120 100  88 117  0.244236    991/1000    NonOverlappingTemplate
106 101 102 101  94  89 105  96 109  97  0.951205    988/1000    NonOverlappingTemplate
103  88  99 110  88 102 101  96 118  95  0.566688    987/1000    NonOverlappingTemplate
112 116  97 105 108  97  92  94  94  85  0.467322    992/1000    NonOverlappingTemplate
 92  94 108  91 114  95 115 103  90  98  0.530120    995/1000    NonOverlappingTemplate
 89 103 112 104 120 107  92  89 109  75  0.060875    996/1000    NonOverlappingTemplate
107  83 101 102 114 102 104  89 103  95  0.622546    993/1000    NonOverlappingTemplate
 93 106  92  91  90 104 122 118  98  86  0.139655    991/1000    NonOverlappingTemplate
101  99  99  93 106 102 104  99  98  99  0.999041    991/1000    NonOverlappingTemplate
101 109  88 116  98  93 109  93  93 100  0.622546    991/1000    NonOverlappingTemplate
 85 106 122 114  96 103  83 110  73 108  0.010681    987/1000    NonOverlappingTemplate
 93  96 103 103 102  95 108 101 112  87  0.842937    990/1000    NonOverlappingTemplate
 89 106 107  94 110  93 104 101  99  97  0.899171    990/1000    NonOverlappingTemplate
102 101  90 106 107 111  88 105  94  96  0.805569    988/1000    NonOverlappingTemplate
117  92  90  96  92  93 115 104  98 103  0.498313    987/1000    NonOverlappingTemplate
 82  97  92 112 108  97 101  89 110 112  0.366918    991/1000    NonOverlappingTemplate
 94  88 111 107  95  85 107 103 108 102  0.610070    990/1000    NonOverlappingTemplate
 87 102  88 127  94  98 114  99  87 104  0.100109    993/1000    NonOverlappingTemplate
 87 115  94  89 108  94 100 112  92 109  0.401199    988/1000    NonOverlappingTemplate
 95 107  98  95  94  90 121 113  99  88  0.355364    991/1000    NonOverlappingTemplate
 76  98  94 118 108 105  87 120 106  88  0.037813    993/1000    NonOverlappingTemplate
107 109 112 105  87 101  82 101 107  89  0.380407    988/1000    NonOverlappingTemplate
107  96  98 102  87 117  96  99  95 103  0.757790    986/1000    NonOverlappingTemplate
106  93 108 100 112  89 101  99  95  97  0.875539    988/1000    NonOverlappingTemplate
114  98  91 105  95 115  93 113  90  86  0.296834    986/1000    NonOverlappingTemplate
 91  96 111 101  99  87 119  91 105 100  0.478839    988/1000    NonOverlappingTemplate
110 100 109 105 100  97 102  94  91  92  0.911413    987/1000    NonOverlappingTemplate
101 114  95 110  82  97 105 102  95  99  0.626709    994/1000    NonOverlappingTemplate
106  97  98  99  85 102 123  98  90 102  0.422638    990/1000    NonOverlappingTemplate
 96 101  96  96  90 132 100  87  97 105  0.131122    992/1000    NonOverlappingTemplate
112 124  81  90 106 103  72 101 106 105  0.013953    983/1000    NonOverlappingTemplate
108 114  91 104  91 111  98  97  96  90  0.649612    988/1000    NonOverlappingTemplate
102  84 106 105 114  95  85  93 110 106  0.390721    990/1000    NonOverlappingTemplate
108 107 117  99  91  98  77  92 111 100  0.212184    993/1000    NonOverlappingTemplate
116 105  97  88 112 100 105  97  96  84  0.452173    985/1000    NonOverlappingTemplate
107  97  94  98 115 112  99  97 112  69  0.070737    990/1000    NonOverlappingTemplate
 98 101 108 114  98  87 114  87  96  97  0.506194    995/1000    NonOverlappingTemplate
 83  91  97 108  99 102  96 106 118 100  0.510153    995/1000    NonOverlappingTemplate
110  84 103  97 103  79 101 105 118 100  0.228367    983/1000    NonOverlappingTemplate
102 117 108 103 104 103  85  98  89  91  0.512137    989/1000    NonOverlappingTemplate
 98  84 102  86 104 111  99 113 100 103  0.558502    989/1000    NonOverlappingTemplate
106 110 101  90  97 107 109 110  92  78  0.331408    989/1000    NonOverlappingTemplate
 82  92 105 107  83 101 108 106 107 109  0.365253    987/1000    NonOverlappingTemplate
 99 111  98  98  93 104  90  98 105 104  0.946308    991/1000    NonOverlappingTemplate
 99  88  83  95 109 110 119  92  91 114  0.144504    992/1000    NonOverlappingTemplate
111  84  97  99 100  99 105 101 107  97  0.858002    988/1000    NonOverlappingTemplate
 85  91  89  95 116 105  93 118 109  99  0.220159    990/1000    NonOverlappingTemplate
109 104 114 103  98 108  79  86  85 114  0.112708    993/1000    NonOverlappingTemplate
100 100  98 112 108 105  75  90 105 107  0.322135    987/1000    NonOverlappingTemplate
 93  93  94 108  83  99 104 100 110 116  0.474986    988/1000    NonOverlappingTemplate
101 123 122 105  90 106  76 101  88  88  0.015598    991/1000    NonOverlappingTemplate
104  91  95 108 109  99  99  96  92 107  0.912724    984/1000    NonOverlappingTemplate
 92  83 104 101  97 115  96 110 101 101  0.614226    987/1000    NonOverlappingTemplate
106  83  95 102 100  99 109 105 100 101  0.866097    993/1000    NonOverlappingTemplate
 91 106  85 109  97  91 102 114 101 104  0.605916    993/1000    NonOverlappingTemplate
 89  96  92 109  98 109 104 107  96 100  0.877083    992/1000    NonOverlappingTemplate
122 109 112  82  88 100  92  93 105  97  0.152044    987/1000    NonOverlappingTemplate
109  84  91  99  88 105 119  99  92 114  0.207730    989/1000    NonOverlappingTemplate
103 110  84  91 111 102 106  98  91 104  0.628790    992/1000    NonOverlappingTemplate
 96 118  96  83  99 103 117  91  94 103  0.296834    993/1000    NonOverlappingTemplate
 89 111  98 113  93 101 101 102  96  96  0.832561    987/1000    NonOverlappingTemplate
100 107  84 103 101  96  97 107 117  88  0.512137    996/1000    NonOverlappingTemplate
115  88 116  88  84  98 120  91  99 101  0.087692    985/1000    NonOverlappingTemplate
 98  90 111 103  94  98 103  93  99 111  0.872425    991/1000    NonOverlappingTemplate
115  96  97 100 110 105  90  96  97  94  0.801865    988/1000    NonOverlappingTemplate
100 131 105  94  87  99  93  96  95 100  0.170922    990/1000    NonOverlappingTemplate
114 106  91  95  95 111  88  91  97 112  0.473064    989/1000    NonOverlappingTemplate
105  90  97 102 109  94  90 110 101 102  0.867692    986/1000    NonOverlappingTemplate
102 107 106  90 103  92  98 110 106  86  0.741918    985/1000    NonOverlappingTemplate
111  86  83  81 113 104 108 104 109 101  0.156373    983/1000    NonOverlappingTemplate
 87 109  88 106  95 112  99 101 103 100  0.729870    992/1000    NonOverlappingTemplate
103 102  90  97 101 109  85 123  89 101  0.289667    988/1000    NonOverlappingTemplate
 91  88  85 109 113 106  87 117  83 121  0.023228    991/1000    NonOverlappingTemplate
 93  95  98  92 115 118  76  96 108 109  0.112708    995/1000    NonOverlappingTemplate
 99 103  84  82 101 106 105 123 104  93  0.188601    986/1000    NonOverlappingTemplate
114  93  98 103  98  96  92  97 106 103  0.914025    988/1000    NonOverlappingTemplate
109 116  75  98 100 112 103  96 101  90  0.193767    989/1000    NonOverlappingTemplate
108  93 106  90 108  83 110  88 104 110  0.382115    984/1000    NonOverlappingTemplate
 97  89 100  97 107  89 109 108 117  87  0.426272    991/1000    NonOverlappingTemplate
111  78 101  99 106 110 105  96 113  81  0.156373    993/1000    NonOverlappingTemplate
105 111  93  76 110 100  93  89 108 115  0.149495    983/1000    NonOverlappingTemplate
 99 109  97 103 101 100  91  96 103 101  0.990138    990/1000    NonOverlappingTemplate
 86  90  92 105 113 117  98  88 109 102  0.292519    994/1000    NonOverlappingTemplate
 93  99  92 110 105 101  88 102 103 107  0.878618    993/1000    NonOverlappingTemplate
102  99 112  94 107  91 108  90  83 114  0.380407    992/1000    NonOverlappingTemplate
114 111  85  97  98 106  95 101  78 115  0.151190    993/1000    NonOverlappingTemplate
 94 105 110 107  91  95 104 106  98  90  0.858002    987/1000    NonOverlappingTemplate
 98 114 109  95 102 104  90  95 100  93  0.834308    988/1000    NonOverlappingTemplate
102 118  97 108  69 105  95  99 102 105  0.108150    992/1000    NonOverlappingTemplate
110 104  79  92 107 103  89 111 106  99  0.385543    989/1000    NonOverlappingTemplate
113  95 106 106 106  96  87 112  90  89  0.482707    984/1000    NonOverlappingTemplate
106 120  82  93  76 126  85  80 115 117  0.000199    986/1000    NonOverlappingTemplate
 96 100  93  98 105  98  95 103 109 103  0.987492    991/1000    NonOverlappingTemplate
 95 107  98  95  96  88 122 112  99  88  0.322135    991/1000    NonOverlappingTemplate
103 108 119  91  71  93  93 112 102 108  0.054314    987/1000    OverlappingTemplate
127  88  86 108 103  89  96  98  94 111  0.108791    986/1000    Universal
102 117 104  90 105  99 101  99  93  90  0.753844    994/1000    ApproximateEntropy
 60  48  42  52  62  59  58  62  51  71  0.262485    561/565     RandomExcursions
 53  51  63  52  72  56  51  61  63  43  0.267238    558/565     RandomExcursions
 69  57  51  54  60  50  52  60  55  57  0.826457    561/565     RandomExcursions
 60  68  42  66  57  61  64  51  52  44  0.160197    562/565     RandomExcursions
 72  55  52  46  55  51  52  67  64  51  0.264854    559/565     RandomExcursions
 49  48  61  56  56  72  51  50  65  57  0.384746    557/565     RandomExcursions
 69  44  47  52  61  53  54  60  69  56  0.255473    556/565     RandomExcursions
 59  68  65  50  61  55  53  43  60  51  0.419343    552/565     RandomExcursions
 55  57  59  43  55  56  63  64  51  62  0.705539    558/565     RandomExcursionsVariant
 58  50  60  54  60  63  50  48  52  70  0.554056    559/565     RandomExcursionsVariant
 60  53  53  53  65  57  53  64  45  62  0.705539    560/565     RandomExcursionsVariant
 55  53  56  59  66  58  45  64  60  49  0.672396    556/565     RandomExcursionsVariant
 49  50  63  56  53  75  58  57  60  44  0.216346    557/565     RandomExcursionsVariant
 51  61  53  51  66  60  59  65  51  48  0.661274    558/565     RandomExcursionsVariant
 52  69  64  45  53  55  60  64  44  59  0.279399    562/565     RandomExcursionsVariant
 47  65  62  65  63  41  60  54  58  50  0.279399    561/565     RandomExcursionsVariant
 58  61  50  44  66  64  51  60  60  51  0.521600    558/565     RandomExcursionsVariant
 54  50  55  62  47  63  51  69  57  57  0.616677    559/565     RandomExcursionsVariant
 49  71  48  60  51  64  49  57  57  59  0.438924    560/565     RandomExcursionsVariant
 63  60  59  57  50  56  60  55  56  49  0.957581    563/565     RandomExcursionsVariant
 64  66  55  53  56  58  44  57  55  57  0.759405    561/565     RandomExcursionsVariant
 61  62  60  60  59  51  57  54  55  46  0.900520    560/565     RandomExcursionsVariant
 65  53  62  59  54  54  53  53  65  47  0.762909    560/565     RandomExcursionsVariant
 60  62  58  47  64  50  54  48  63  59  0.690860    560/565     RandomExcursionsVariant
 64  53  54  57  65  54  52  55  52  59  0.932293    561/565     RandomExcursionsVariant
 58  62  56  54  59  55  54  50  54  63  0.977331    561/565     RandomExcursionsVariant
100 117  98 101 103  94  93 104  92  98  0.858002    993/1000    Serial
102 102  93  88 106 107  99  98  93 112  0.848027    997/1000    Serial
108  93  89  97  99 106 104 101 103 100  0.961869    986/1000    LinearComplexity


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
The minimum pass rate for each statistical test with the exception of the
random excursion (variant) test is approximately = 980 for a
sample size = 1000 binary sequences.

The minimum pass rate for the random excursion (variant) test
is approximately = 552 for a sample size = 565 binary sequences.

For further guidelines construct a probability table using the MAPLE program
provided in the addendum section of the documentation.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

```

####
---
* [previous: Linear Regression](./linear_regression.md)

